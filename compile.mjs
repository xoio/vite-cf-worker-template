import fs from "fs"
import {build} from "vite"
import path from "path"
import {fileURLToPath} from "url";


const __dirname = fileURLToPath(new URL('.', import.meta.url))

async function compile() {
    const output_dest = path.resolve(__dirname, "output")

    await build({
        configFile: path.resolve(__dirname, "vite.config.ts"),
        build: {
            minify: true,
            cssMinify: true,
            outDir: output_dest
        },
    })

    fs.cpSync(path.resolve(__dirname, "workers"), path.resolve(output_dest,"workers"), {recursive: true})
    fs.cpSync(path.resolve(__dirname, "wrangler.toml"), path.resolve(output_dest, "wrangler.toml"))
}

compile()