//@ts-nocheck
import {getAssetFromKV, mapRequestToAsset} from "@cloudflare/kv-asset-handler";
import manifestJSON from '__STATIC_CONTENT_MANIFEST';

const assetManifest = JSON.parse(manifestJSON);
const fileExtensionRegex = /\.(pdf|txt|jpg|jpeg|png|gif|mp3|mp4|js|css|woff|ttf|woff2|otf)$/i;

/**
 * Rewrites the url as necessary
 * @param request {Request} the request object coming in. Remember this is for all requests.
 * @returns {Request}
 */
const urlRewrite = request => {
    let url = request.url

    // if request is a file extension, just let it pass through
    if (fileExtensionRegex.test(url)) {
        return mapRequestToAsset(new Request(url, request))
    }

    // from here, we just want to make sure that all other requests load the index page.
    // most routing will get handled by the javascript
    let c = new URL(request.url)

    return mapRequestToAsset(new Request(c.origin, request))
}
export default {
    async fetch(req, env, ctx) {

        try {
            return await getAssetFromKV({
                request: req,
                waitUntil: ctx.waitUntil.bind(ctx)

            }, {
                mapRequestToAsset: urlRewrite,
                ASSET_MANIFEST: assetManifest,
                ASSET_NAMESPACE: env.__STATIC_CONTENT
            })
        } catch (e) {
            return new Response("bad")
        }
    }
}