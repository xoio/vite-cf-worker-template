Vite Cloudflare worker example
===

A simple sample of how to integrate Cloudflare Workers with a static Vite project.
This particular example includes use of Svelte. Note that this is intended for small one-page static sites, 
if you need something more fully featured,  one of the many frameworks like [Hono](https://hono.dev/) may be more appropriate if not
[Cloudflare Pages](https://pages.cloudflare.com/).

Folder structure
===
* `src` - where app files should go. 
* `workers` - where your worker files go
* `public` - all of your static assets.
* `wrangler.toml` - the Wrangler settings to use.

